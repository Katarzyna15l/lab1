﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Lab1.Main
{
    public class Program
    {
        static void Main(string[] args)
        {

            List<Contract.IMundurowy> cos = new List<Contract.IMundurowy>();
            cos.Add(new Implementation.Impl1());
            cos.Add(new Implementation.Impl2());
            cos.Add(new Implementation.Impl1());

            foreach (var item in cos)
            {
                Console.WriteLine(item.Zadanie());
            }
            Console.ReadKey();
        }
    }
}
